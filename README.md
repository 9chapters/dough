# The dough

<img src="img/dough_logo.png" alt="drawing" width="250"/>

The **dough** makes your daily life easier by letting you interact with tezos contract easier. It provides a _command line tool_ and _haskell library_ for interacting with tezows-client.

Currently **dough** is developed specifically for supporting [teznames](https://gitlab.com/tezos-southeast-asia/teznames). The internal library and some partical functions like command `dough data` will be extended for more general uses in the future.

## Install, Setup and Usage

For installation and usage, please read [our documents](https://dough.readthedocs.io/)!

## Contributing

Thanks for your interest in dough. Here are some ways you can get involved and help us improve the dough!

### to contribute doc

The [Users guide](https://dough.readthedocs.io/) is in branch `docs`. If you're willing to help us to improve our document and users guide, please fork and create a branch for editing. Once you finish you work, please issue an _merge request_ to our `docs` branch. We will review and merge it asap.

### to contribute code

So far the main developing branch is still `master`. If you're willing to help us on improving our source code, please fork and create a branch for editing. Once you finish you work, please issue an _merge request_ to our `master` branch. We will review and merge it asap.

### report bug

If you found any error or bug, please kindly inform us. All you need is to go to [dough/issues](https://gitlab.com/9chapters/dough/issues) page to open a new issue and tag it with the preseted Label: `bug`!

### feedback and feature suggestion

If there is some feature you need or you just want to give some feedback, please go to [dough/issues](https://gitlab.com/9chapters/dough/issues) page to open a new issue and tag it with the preseted Label: `discussion` (for feedback) or `suggestion` (for features)!
