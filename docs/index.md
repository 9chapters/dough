<img src="img/dough_logo.png" alt="drawing" width="250"/>

The **dough** makes your daily life easier by letting you interact with tezos contract easier. It provides a _command line tool_ and _haskell library_ for interacting with tezos-client.

!!! warning

    Currently **dough** is developed specifically for supporting [teznames](https://gitlab.com/tezos-southeast-asia/teznames). The internal library and some partical functions like command `dough data` will be extended for more general uses in the future.
