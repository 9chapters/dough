As in early developing stage, **dough** is specifically developed as for [teznames](https://gitlab.com/tezos-southeast-asia/teznames) ([spec](https://teznames.readthedocs.io/en/latest/system/spec/)) only. This page contains and explains all the commands written for deploying and interacting with teznames!

## Command: `inquire`

!!! info "Command"

    ```bash
    dough inquire [-c <path_to_file>] -n <contract_alias>
    ```

To show the storage of a given _tezname contract_. It contains two arguments:

+ [optional] `-c <path_to_config_file>` dough's globol config file
+ [necessary] `-n <contract_alias>` alias of targeting tezname contract

The first, `-c`, is still for dough config. The other one, `-n`, is for the alias of targeting tezname contract.

## Command: `data`

!!! info "Command"

    ```bash
    dough data [-c <path_to_config_file>] -d <path_to_input_file>  [-o <path_to_output_file>]
    ```

One way to use **dough** is to use it as a _prepared-for-tezos-client_ Michelson data generator. `dough data` can translate a given Haskell data into one-lined Michelson data that is required for deploying or invoking a tezos contract manually. `dough data` contains three arguments as follows.

 + [optional] `-c <path_to_config_file>` dough's globol config file
 + [necessary] `-d <path_to_input_file>` input file (in terms of Haskell's record structure)
 + [optional] `-o <path_to_output_file>` output file (in terms of one-line Michelson data)

Due to the fact that **dough** supports _teznames_ only, the input data format is fixed and must follow
the structure defined in the [specification of cTezName's storage](https://teznames.readthedocs.io/en/latest/system/spec/#contract-storage).

??? example

    ```haskell
    TzNameData
       { sProtocol  = "9chsTNS-0.0.1"
       , sTzName    = "myAbpS"
       , sAvailable = "False"
       , sOwner = TNID "abpS"
       , sAdmin = TNID "dotblack"
       , sDNS =
          [ ("dns1.tzname.org", "192.168.1.1")
          , ("dns2.tzname.org", "192.168.1.2") ]
       , sAppliedDate      =  "2019-01-01T09:01:00Z"
       , sExpireDate       =  "2020-01-01T09:01:00Z"
       , sLastModification = ("2019-05-25T09:03:48Z", "registered")
       , sRoot = "tz1bhXKVY4ihH8Dcao4cuk8KxJ4sPjXGZcEp"
       }
    ```

If the output file wasn't specified by `-o`, the result information will be displayed on screen.

## Command: `deploy`

!!! info "Command"

    ```bash
    dough deploy [-c <path_to_config_file>] -d <path_to_input_file> -i <path_to_.tz_file> -n <contract_alias>
    ```

To deploy or originate a tezname contract.

 + [optional] `-c <path_to_config_file>` **dough**'s globol config file
 + [necessary] `-d <path_to_input_file>` input file (in terms of Haskell's record structure)
 + [necessary] `-i <path_to_.tz_file>` the path to source contract (.tz)
 + [necessary] `-n <contract_alias>` the alias of TzName contract

The input file format and detail is the same as used in `dough data`.

## Command: `invoke`

!!! info "Command"

    ```bash
    dough invoke [-c <path_to_config_file>] -n <contract_alias> -i <path_to_input_file>
    ```

To call one of the entries on a deployed tezname contract.

 + [optional] `-c <path_to_config_file>` **dough**'s globol config file
 + [necessary] `-n <contract_alias>` the alias of TzName contract
 + [necessary] `-i <path_to_input_file>` input data file as in Haskell record type

The `-i` is for giving **dough** a Haskell's record structure for expressing which entry you want to use. In fact, Michelson use algebraic data type to realize multi-entries. And for every entry, there is an unique parameters declaration. So, **dough** need a two-layered data structures to reflect the information required by this invoking. To be able to do so, we define

``` haskell
 data InvokeArgs = InvokeArgs
    { ivkTransfer :: Int
    , ivkInfo     :: InkEntry
    } deriving Read
```

where `ivkTransfer` is the field to indicate the quantity of transferring tokens; and, the `ivkInfo` field is reflectting which entries we are calling and all the required parameters. The `InkEntry` is defined as follows.

``` haskell
data InkEntry
   = TNS_Register    { newOwner :: T.Text}
   | TNS_Renew
   | TNS_Free
   | TNS_UpdateRoot  { newRoot :: T.Text }
   | TNS_updateAdmin { newAdmin :: T.Text }
   deriving (Show, Read)
```

Each of those five data contractors reflects one entry of tezname contract. You can read the [cTezName's specification](https://teznames.readthedocs.io/en/latest/system/spec/#contract-entry) for understanding.

??? example

    ```haskell
    InvokeArgs
        { ivkTransfer = 0
        , ivkInfo     = TNS_Renew
        }
    ```
    ```haskell
    InvokeArgs
        { ivkTransfer = 0
        , ivkInfo     =
          TNS_Register
             { newOwner = "abpH" }
        }
    ```
    ```haskell
    InvokeArgs
        { ivkTransfer = 0
        , ivkInfo     = TNS_Free
        }
    ```
