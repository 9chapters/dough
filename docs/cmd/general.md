All general-purposed _dough_ commands are listed in this page.

## Command: `show`

!!! info "Command"

    ```bash
    dough show [-c <path_to_file>] [-r]
    ```

To list all of the local TZ accounts which tezos-clinet can actually get secrete key. It has two optional arguments as follows.

+ [optional] `-c <path_to_config_file>` dough's globol config file
+ [optional] `-r` right alignment

If the `-c` argument was not specified, **dough** will look for a file named `dough.config` located in user's home directory.
