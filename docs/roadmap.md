!!! info "roadmap and release plan"

    + The end of **June** 2019
        - get user guide (for PoC) ready
    + The 1st half of **July** 2019
        - support tezname for restricted name list
        - support tezname for premium name list
    + The 2st half of **July** 2019
        - support tezname for auction (or taxing)
        - support tezname for sub-tezname deployment
    + The 1st half of **August** 2019
        - support tezname for revealing more different kinds of contract storage
    + The 2st half of **August** 2019
        - all necessary supports for tezname
        - generalise `dough data` into universal version
    + The 1st half of **September** 2019
        - TBA
    + The 2st half of **September** 2019
        - TBA

!!! check "todo"

    - [x] [doc] contribute notes
    - [x] [doc] add more explanations of the input record data (required by `data`, `deploy` and `invoke`)
    - [x] [doc] add more contents on page _Setup_
    - [x] [doc] add more contents on page _Commands_
    - [ ] [release] review and confirm coverage of all documentations
    - [ ] [release] pre-built binary for download directly
    - [ ] [update] inquire whois-ish info of a tezname contract by `dough inquire -n <name>` or `-n <address>`
    - [ ] [new] new command `tc` for type-checking _.tz_
