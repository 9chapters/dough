
The **dough** is a command line tool written in Haskell. To compile **dough**, you need to install [Stack](https://docs.haskellstack.org/en/stable/README/) on your computer. If you don't have it, please follow this [how to install](https://docs.haskellstack.org/en/stable/README/#how-to-install) to install it.

Once you get Stack ready, then just follow the following instructions.

```bash
git clone git@gitlab.com:9chapters/dough.git
cd dough
stack build
stack install
```

After installing, it's very important to setup a globol config file. You can read more detail on [Configuration](https://dough.readthedocs.io/en/latest/setup/config/).
