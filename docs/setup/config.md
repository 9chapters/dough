
To run **dough**, you need a config file as system configuration. It's required to be written in terms of haskell's record structure as in the following example.

!!! info "Config exmaple"

    ``` haskell
    ClientConfig
       { nodeAddress = "node1.lax.tezos.org.sg"
       , who         = "dotblack"
       , burnCap     = 10
       , ptclTNS     = "9chsTNS-0.0.1"
       , ptclTNRS    = "9chsTNRS-0.0.1"
       , cTNRSAddr   = "KT1P4fir3MtexzztWbybNxonCv28Fu6hwmGA"
       }
    ```

It contains 2 parts. The 1st part contains 3 fields and is for _tezos-client_:

+ `nodeAddress` - the node address
+ `who` - the role who **dough** is acting as
+ `burnCap` - how many _burn-cap_ you want to setup for deploying or interacting with a contract.

The 2nd part includes 3 fields and is for [teznames](https://gitlab.com/tezos-southeast-asia/teznames) ([doc](https://teznames.readthedocs.io/)) only:

+ `ptclTNS` - the protocol version of teznames
+ `ptclTNRS` - the protocol version of teznames record contract
+ `cTNRSAddr` - address of tezname record contract

!!! tip

    The path to this config file can be passed to any _dough commands_ as its first argument. For example,

    ``` bash
    dough show -c /dir1/dir2/dough.config
    ```

    Or, if it was not specified, **dough** will assume it's located in the _home_ directory and named as `dough.config`.
