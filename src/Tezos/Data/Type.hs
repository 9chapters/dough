module Tezos.Data.Type where
--
data MiType
   = MiType_Address
   | MiType_Bytes
   | MiType_Key
   | MiType_KeyHash
   | MiType_Signature
   | MiType_String -- timestamp included
   | MiType_Unit
   | MiType_Num
   | MiType_Bool
   deriving (Show, Eq)
--
data Expr a
   = Nest [Expr a]
   | Pair (a, a)
   | Atom a
   | HashMap [(a, a)]
   deriving (Show, Eq, Read)
--
