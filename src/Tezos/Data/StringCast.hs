{-# LANGUAGE OverloadedStrings #-}
--
module Tezos.Data.StringCast where
--
import Tezos.Data.Type
--
import Data.Char (isDigit)
import qualified Data.Text as T
import Data.Maybe (mapMaybe)
--
class MiData a where
   attachType :: a -> (MiType, a)
   toMichelsonFormat :: MiType -> a -> a
--
instance MiData Bool where
   attachType t = (MiType_Bool, t)
   toMichelsonFormat _ = id
--
instance MiData Int where
   attachType t = (MiType_Num, t)
   toMichelsonFormat _ = id
--
instance MiData () where
   attachType () = (MiType_Unit, ())
   toMichelsonFormat _ = id
--
instance MiData T.Text where
   attachType t
      | isTZ      t = (MiType_Address, t)
      | isKT      t = (MiType_Address, t)
      | isBytes   t = (MiType_Bytes, t)
      | isKey     t = (MiType_Key, t)
      | isKeyHash t = (MiType_KeyHash, t)
      | isSig     t = (MiType_Signature, t)
      | isBool    t = (MiType_Bool, t)
      | isUnit    t = (MiType_Unit, t)
      | isNum     t = (MiType_Num, t)
      | otherwise   = (MiType_String, t)
   --
   toMichelsonFormat MiType_Address   t = T.concat ["\"", t, "\""]
   toMichelsonFormat MiType_Key       t = T.concat ["\"", t, "\""]
   toMichelsonFormat MiType_KeyHash   t = T.concat ["\"", t, "\""]
   toMichelsonFormat MiType_Signature t = T.concat ["\"", t, "\""]
   toMichelsonFormat MiType_String    t = T.concat ["\"", t, "\""]
   toMichelsonFormat MiType_Bytes     t = t
   toMichelsonFormat MiType_Bool      t = case t of
      "false" -> "False"
      "False" -> "False"
      "true" -> "True"
      "True" -> "True"
      _ -> error "not a bool at all"
   toMichelsonFormat MiType_Num       t = t
   toMichelsonFormat MiType_Unit      t = t
   --
--
isTZ      t = T.isPrefixOf "tz" t
isKT      t = T.isPrefixOf "KT" t
isBytes   t = T.isPrefixOf "0x" t
isKey     t = T.isPrefixOf "edp" t
isKeyHash t = T.isPrefixOf "tz" t
isSig     t = T.isPrefixOf "edsig" t
isBool    t =  T.isPrefixOf "false" t
            || T.isPrefixOf "False" t
            || T.isPrefixOf "true" t
            || T.isPrefixOf "True" t
isUnit    t = T.isPrefixOf "unit" t
isNum     t = and $ map isDigit $ T.unpack t
--
michelsonDataRender :: (Expr T.Text) -> T.Text
michelsonDataRender (Atom t) = michelsonFormatWithType t
michelsonDataRender (Pair (a, b)) = T.concat
   ["(Pair ", michelsonFormatWithType a, " ", michelsonFormatWithType b, ")"]
michelsonDataRender (HashMap xs) = hashMapRender xs ""
michelsonDataRender (Nest []) = ""
michelsonDataRender (Nest [t]) = michelsonDataRender t
michelsonDataRender (Nest (t:ts)) = T.concat
   ["(Pair ", michelsonDataRender t, " ", michelsonDataRender (Nest ts), ")"]
--
michelsonFormatWithType :: (MiData a) => a -> a
michelsonFormatWithType = (uncurry toMichelsonFormat) . attachType
--
hashMapRender :: [(T.Text, T.Text)] -> T.Text -> T.Text
hashMapRender [] res = T.concat ["{ ", T.dropEnd 3 res, " }"]
hashMapRender ((k,v):xs) res =
   hashMapRender xs $ T.concat [res, "Elt \"", k, "\" \"", v, "\" ; "]
--
getPackedData :: T.Text -> T.Text
getPackedData raw =
   let line = T.words $ head $ T.lines raw in
   case line of
      ("Raw" : "packed" : "data:" : d : []) -> d
      _ -> error "hash has been failed"
--
getAddrFromOrigination :: T.Text -> T.Text
getAddrFromOrigination raw =
   let ctx = map T.words $ T.lines raw in
   case mapMaybe pick ctx of
      [] -> error "hash has been failed"
      (x:_) -> x

pick :: [T.Text] -> Maybe T.Text
pick ("New" : "contract" : kt : "originated." : _ ) = return kt
pick _ = Nothing
