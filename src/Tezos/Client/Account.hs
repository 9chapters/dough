{-# LANGUAGE OverloadedStrings #-}
--
module Tezos.Client.Account where
--
import qualified Data.Maybe as Maybe
import qualified Data.Text as T
--
data TZ = TZ
   { tz1Name :: T.Text
   , tz1Addr :: T.Text
   , tz1Key  :: T.Text
   } deriving (Eq)
instance Show TZ where
   show (TZ n a k) = "tz::"++(show n)++"<"++(show k)++">"
--
--
genTZ :: T.Text -> [[T.Text]] -> Maybe TZ
genTZ name xs = do
   let hash = head $ Maybe.catMaybes $ map getHash xs
   let key  = head $ Maybe.catMaybes $ map getPublicKey xs
   return $ TZ name hash key
--
getKeyfromTZs :: [TZ] -> T.Text -> Maybe T.Text
getKeyfromTZs tzs name
   = Maybe.listToMaybe
   $ map tz1Key
   $ filter ((name ==).tz1Name) tzs

-- > tezos-client -A node1.lax.tezos.org.sg show address dotblack -S
-- Hash: tz1bhXKVY4ihH8Dcao4cuk8KxJ4sPjXGZcEp
-- Public Key: edpkuesJWtpYgjM1wjxHn7oiJsT64rMrGXztoa7eQ9MGRCL2GtEm21
-- Secret Key: unencrypted:edsk35ZbujnCeZJ9dfnk9H95tsS3KC747vX8pyBKTrGa9BDrX2UJBh
--
getHash :: [T.Text] -> Maybe T.Text
getHash ("Hash:" : k : _) = Just k
getHash _ = Nothing
--
getPublicKey :: [T.Text] -> Maybe T.Text
getPublicKey ("Public" : "Key:" : k : _) = Just k
getPublicKey _ = Nothing
--
getPrivateKey :: [T.Text] -> Maybe T.Text
getPrivateKey ("Secret" : "Key:" : k : _) = Just k
getPrivateKey _ = Nothing
--
getAccSK :: [T.Text] -> Maybe T.Text
getAccSK (name : _ : "(unencrypted" : "sk" : "known)" : _) = Just (T.init name)
getAccSK _ = Nothing
--
