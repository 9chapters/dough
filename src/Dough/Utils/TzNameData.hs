{-# LANGUAGE OverloadedStrings #-}
--
module Dough.Utils.TzNameData where
--
import Shelly
import System.Exit
--
import Data.Bifunctor (bimap)
--
import Dough.Args.OptArg
import Tezos.Client
import Tezos.Data
--
import Control.Monad
import Control.Monad.Reader
import Text.Read (readMaybe)
--
import Data.Default
import Data.Maybe (listToMaybe, catMaybes)
import qualified Data.Text as T
--
data TzNameData = TzNameData
   { sProtocol    :: T.Text
   , sTzName      :: T.Text
   , sAvailable   :: T.Text
   , sOwner       :: TzNameIdentity
   , sAdmin       :: TzNameIdentity
   , sDNS         :: [(T.Text, T.Text)]
   , sAppliedDate      :: T.Text
   , sExpireDate       :: T.Text
   , sLastModification :: (T.Text,T.Text)
   , sRoot             :: T.Text
   } deriving (Read)
--
instance Show TzNameData where
   show tnd =
      "\ESC[37m\STX    sProtocol\ESC[0m\STX  = " ++ (T.unpack $ sProtocol tnd) ++ "\n" ++
      "\ESC[32m\STX    sTzName\ESC[0m\STX    = " ++ (T.unpack $ sTzName tnd) ++ "\n" ++
      "\ESC[32m\STX    sAvailable\ESC[0m\STX = " ++ (T.unpack $ sAvailable tnd) ++ "\n" ++
      "\ESC[33m\STX    sOwner\ESC[0m\STX     = " ++ (show $ sOwner tnd) ++ "\n" ++
      "\ESC[33m\STX    sAdmin\ESC[0m\STX     = " ++ (show $ sAdmin tnd) ++ "\n" ++
      "\ESC[37m\STX    sDNS\ESC[0m\STX       = " ++ ((flip showLT) "" $ sDNS tnd) ++ "\n" ++
      "\ESC[35m\STX    sAppliedDate\ESC[0m\STX      = " ++ (T.unpack $ sAppliedDate tnd) ++ "\n" ++
      "\ESC[35m\STX    sExpireDate\ESC[0m\STX       = " ++ (T.unpack $ sExpireDate tnd) ++ "\n" ++
      "\ESC[35m\STX    sLastModification\ESC[0m\STX = " ++ (showPT $ sLastModification tnd)
--
showPT :: (T.Text, T.Text) -> String
showPT (t1, t2) = "("++(T.unpack t1)++", "++(T.unpack t2)++")"
showLT :: [(T.Text, T.Text)] -> String -> String
showLT ((t1, t2) : xs) temp =
   showLT xs ("("++(T.unpack t1)++", "++(T.unpack t2)++"), "++temp)
showLT [] "" = ""
showLT [] temp = "["++(init (init temp))++"]"
--
data TzNameIdentity
   = TNRaw (T.Text, T.Text)
   | TNID T.Text
   deriving (Read)
--
instance Show TzNameIdentity where
   show (TNRaw (k, q)) =
      "\ESC[4;37m\STX"++(T.unpack k)++"\ESC[0m\STX"++
      " (\ESC[4;37m\STX"++(T.unpack q)++"\ESC[0m\STX)"
   show (TNID name) = "<\ESC[34m\STX"++(T.unpack name)++"\ESC[0m\STX>"
--
instance Default TzNameData where
   def = TzNameData
      "9chsTNRS-0.0.1" "" "true"
      (TNRaw ("",""))
      (TNRaw ("",""))
      []
      "" "" ("","") ""
--
tzNameData2Expr :: ClientConfig -> TzNameData -> IO (Expr T.Text)
tzNameData2Expr cf x = do
   a <- (deTZID cf $ sOwner x)
   b <- (deTZID cf $ sAdmin x)
   return $ Nest
      [ Atom (sProtocol x)
      , Atom (sTzName x)
      , Atom (sAvailable x)
      , Pair a
      , Pair b
      , HashMap (sDNS x)
      , Atom (sAppliedDate x)
      , Atom (sExpireDate x)
      , Pair (sLastModification x)
      , Atom (sRoot x)
      ]
--
deTZID :: ClientConfig -> TzNameIdentity -> IO (T.Text, T.Text)
deTZID cf (TNRaw p) = return p
deTZID cf (TNID name) = do
   tzs <- loadAccounts cf
   case (listToMaybe $ map tz1Key $ filter ((name ==).tz1Name) tzs) of
      Nothing -> error $ "There is such account named: "++ (T.unpack name)
      (Just key) -> do
         question <- getNowText cf name
         return (key, question)
--
getNowText :: ClientConfig -> T.Text -> IO T.Text
getNowText cf name = shelly $ print_stdout False $ do
   timenow <- fmap T.init $ run "date" ["+%s"]
   let rawQ = T.concat [name, timenow]
   question <- (flip runReader) cf $ do
      nodeInfo <- specNode
      -- storageInfo <- getStorage
      runTzClient $ nodeInfo <> ["hash", "data", T.concat ["\"",rawQ,"\""], "of", "type", "string"]
   return (getPackedData question)
--
loadData :: String -> IO TzNameData
loadData path = do
   content <- readFile path
   let raw' = (readMaybe content) :: Maybe TzNameData
   case raw' of
      Nothing -> die $
         "\ESC[1;37;41m\STX" ++ "[Cannot parse to TzNameData]" ++ "\ESC[0m\STX"
      (Just raw) -> return raw
--
loadAccounts :: ClientConfig -> IO [TZ]
loadAccounts cf = shelly $ print_stdout False $ do
   ls <- runReader getAllAlias cf
   xs <- sequence $ map ((flip runReader) cf . getKeys) ls
   return
      $ catMaybes
      $ map (uncurry genTZ)
      $ zip ls
      $ map (map T.words . T.lines) xs
--
-- answerQuestion :: ClientConfig -> T.Text -> IO T.Text
-- answerQuestion cf name = shelly $ print_stdout False $ do
--    -- timenow <- fmap T.init $ run "date" ["+%s"]
--    -- let rawQ = T.concat [name, timenow]
--    storageRaw <- (flip runReader) cf $ do
--       nodeInfo <- specNode
--       storageInfo <- getStorage
--       runTzClient $ nodeInfo <> storageInfo <> name
--

--
-- (Pair "9chsTNRS-0.0.1" (Pair "myAbpS" (Pair False (Pair (Pair "edpkuecuybjhNpjDwoZKXg3FrrJBmSEz5FsGQBdo7yCjHnfRs1tpDd" 0x05010000000e6162705331353539303234393933) (Pair (Pair "edpkuesJWtpYgjM1wjxHn7oiJsT64rMrGXztoa7eQ9MGRCL2GtEm21" 0x050100000012646f74626c61636b31353539303235303032) (Pair { Elt "dns1.tzname.org" "192.168.1.1" ; Elt "dns2.tzname.org" "192.168.1.2" } (Pair "2019-01-01T09:01:00Z" (Pair "2020-01-01T09:01:00Z" (Pair (Pair "2019-05-25T09:03:48Z" "registered") "tz1bhXKVY4ihH8Dcao4cuk8KxJ4sPjXGZcEp")))))))))
--
unsafeToTzNameData :: [[T.Text]] -> TzNameData
unsafeToTzNameData wss = TzNameData
   { sProtocol  = (wss !! 0) !! 0
   , sTzName    = (wss !! 1) !! 0
   , sAvailable = (wss !! 2) !! 0
   , sOwner       = TNRaw ((wss !! 3) !! 0, (wss !! 4) !! 0)
   , sAdmin       = TNRaw ((wss !! 5) !! 0, (wss !! 6) !! 0)
   , sDNS         = [ ( (wss !! 7) !! 0, (wss !! 7) !! 1 )
                    , ( (wss !! 7) !! 2, (wss !! 7) !! 3 ) ]
   , sAppliedDate      = (wss !! 8) !! 0
   , sExpireDate       = (wss !! 9) !! 0
   , sLastModification = ((wss !! 10) !! 0, (wss !! 10) !! 1)
   , sRoot             = (wss !! 10) !! 2
   }
--
data TNRecordData
   = TNRecordData [[T.Text]]
   deriving (Read, Show ,Eq)
--
unsafeToTNRecordData :: [[T.Text]] -> TNRecordData
unsafeToTNRecordData x = TNRecordData x
--
storagePreprocess :: T.Text -> [[T.Text]]
storagePreprocess raw
      = map (filter (/="Pair"))
      $ map (filter (/="Elt"))
      $ map T.words $ T.lines
      $ T.filter (/=';')
      $ T.filter (/='{') $ T.filter (/='}')
      $ T.filter (/='(') $ T.filter (/=')')
      $ raw
--
lookupKey :: [TZ] -> T.Text -> Maybe T.Text
lookupKey xs name = fmap tz1Key
   $ listToMaybe
   $ filter ((==name).tz1Name) xs
--
lookupQuestion :: TzNameData -> T.Text -> Maybe T.Text
lookupQuestion s key = do
   (oK, oQ) <- gainIdCtx $ sOwner s
   (aK, aQ) <- gainIdCtx $ sAdmin s
   if (T.concat ["\"",key,"\""]) == oK
      then return oQ
      else if (T.concat ["\"",key,"\""]) == aK
         then return aQ
         else Nothing
--
gainIdCtx :: TzNameIdentity -> Maybe (T.Text, T.Text)
gainIdCtx (TNRaw x) = return x
gainIdCtx (TNID  _) = Nothing
