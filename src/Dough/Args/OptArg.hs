module Dough.Args.OptArg where
--
import Options.Applicative
import Data.Semigroup ((<>))
--
--
data ShowOpt = ShowOpt
   { configPath_show :: String
   , isRightAlign :: Bool }
   deriving (Show, Eq)
--
data InquiryOpt = InquiryOpt
   { configPath_inquiry :: String
   , inqText :: String }
   deriving (Show, Eq)
--
data DeployOpt = DeployOpt
   { configPath_deploy :: String
   , dataFile :: String
   , tzFile :: String
   , nameContract :: String }
   deriving (Show, Eq)
--
data InvokeOpt = InvokeOpt
   { configPath_invoke :: String
   , contName :: String
   , infoPath :: String}
   deriving (Show, Eq)
--
data DataOpt = DataOpt
   { configPath_data :: String
   , inFile :: String
   , outFile :: Maybe String
   }
   deriving (Show, Eq)
--
-- == == == == == == == == == ==
--
showOptParser    :: Parser ShowOpt
showOptParser = ShowOpt <$>
   configParser <*>
   alighRightOptParser
--
inquiryOptParser :: Parser InquiryOpt
inquiryOptParser = InquiryOpt <$>
   configParser <*>
   nameParser
   -- (addressParser <|> keyParser)
--
deployOptParser  :: Parser DeployOpt
deployOptParser = DeployOpt <$>
   configParser <*>
   dataParser <*>
   inputParser <*>
   nameParser
--
invokeOptParser  :: Parser InvokeOpt
invokeOptParser = InvokeOpt <$>
   configParser <*>
   nameParser <*>
   inputParser
--
dataOptParser    :: Parser DataOpt
dataOptParser = DataOpt <$>
   configParser <*>
   dataParser <*>
   outputParser
--
-- == == == == == == == == == ==
--
useIdentityOptParser :: Parser Bool
useIdentityOptParser = switch
   (  long "identity"
   <> help "using (key,bytes) as identity" )
--
alighRightOptParser :: Parser Bool
alighRightOptParser = switch
   (  short 'r'
   <> help "Align account name to right" )
--
inputParser :: Parser String
inputParser = strOption
   (  long "input"
   <> short 'i'
   <> metavar "FILE"
   <> help "path and name of input file" )
--
dataParser :: Parser String
dataParser = strOption
   (  long "data"
   <> short 'd'
   <> metavar "DATA FILE"
   <> help "path and name of originating data file" )
--
outputParser :: Parser (Maybe String)
outputParser = fmap foo $ strOption
   (  long "output"
   <> short 'o'
   <> metavar "FILE"
   <> value ""
   <> help "path and name of output file" )
   where
      foo "" = Nothing
      foo x = Just x
--
nameParser :: Parser String
nameParser = strOption
  (  long "name"
  <> short 'n'
  <> metavar "NAME"
  <> help "terget name" )
--
identityParser :: Parser String
identityParser = strOption
   (  long "name"
   <> short 'n'
   <> metavar "NAME"
   <> help "terget name" )
--
addressParser :: Parser String
addressParser = strOption
  (  long "addr"
  <> short 'a'
  <> metavar "KT-ADDR"
  <> help "KT address" )
--
keyParser :: Parser String
keyParser = strOption
  (  long "edp-key"
  <> short 'k'
  <> metavar "edp-KEY"
  <> help "edp-key" )
--
configParser :: Parser String
configParser = strOption
   (  long "config"
   <> short 'c'
   <> metavar "CONFIGFILE"
   <> value ""
   <> help "path of config file" )
--
class Configable a where
   getCFPath :: a -> String
--
instance Configable ShowOpt where
   getCFPath = configPath_show
--
instance Configable InquiryOpt where
   getCFPath = configPath_inquiry
--
instance Configable DeployOpt where
   getCFPath = configPath_deploy
--
instance Configable InvokeOpt where
   getCFPath = configPath_invoke
--
instance Configable DataOpt where
   getCFPath = configPath_data
--
