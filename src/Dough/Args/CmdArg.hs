module Dough.Args.CmdArg where
--
import Dough.Args.OptArg
--
import Options.Applicative
import Data.Semigroup ((<>))
--
opts :: ParserInfo Command
opts = info (args <**> helper) idm
--
args :: Parser Command
args = subparser
   (  cmdShow
   <> cmdUnquiry
   <> cmdDeploy
   <> cmdInvoke
   <> cmdData
   )
--
cmds f = foldl1 (<>) . map ((flip command) f)
--
cmdShow = cmds (info showCmdParser (progDesc "cmd:show"))
   ["show"]
cmdUnquiry = cmds (info inquiryCmdParser (progDesc "cmd:inquiry"))
   ["inquire", "id", "storage"]
cmdDeploy = cmds (info deployCmdParser (progDesc "cmd:deploy"))
   ["deploy"]
cmdInvoke = cmds (info invokeCmdParser (progDesc "cmd:invoke"))
   ["invoke"]
cmdData = cmds (info dataCmdParser (progDesc "cmd:data"))
   ["data"]
--
data Command
  = ShowCmd    ShowOpt
  | InquiryCmd InquiryOpt
  | DeployCmd  DeployOpt
  | InvokeCmd  InvokeOpt
  | DataCmd    DataOpt
  deriving (Show, Eq)
--
showCmdParser    :: Parser Command
inquiryCmdParser :: Parser Command
deployCmdParser  :: Parser Command
invokeCmdParser  :: Parser Command
dataCmdParser    :: Parser Command
showCmdParser    = ShowCmd    <$> showOptParser
inquiryCmdParser = InquiryCmd <$> inquiryOptParser
deployCmdParser  = DeployCmd  <$> deployOptParser
invokeCmdParser  = InvokeCmd  <$> invokeOptParser
dataCmdParser    = DataCmd    <$> dataOptParser
--
instance Configable Command where
   getCFPath (ShowCmd opt) = getCFPath opt
   getCFPath (InquiryCmd opt) = getCFPath opt
   getCFPath (DeployCmd opt) = getCFPath opt
   getCFPath (InvokeCmd opt) = getCFPath opt
   getCFPath (DataCmd opt) = getCFPath opt
--
