{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}

--
module Dough.Handler.ShowHandler where
--
import Shelly
--
import System.IO
import Text.Show.Pretty
--
import Control.Monad
import Control.Monad.Reader
--
import Tezos.Data
import Tezos.Client
--
-- import Dough.Args.CmdArg
import Dough.Args.OptArg
-- import Dough.Args.Config
--
import qualified Data.Text as T
import qualified Data.Maybe as Maybe
--
showHandler :: ClientConfig -> ShowOpt -> IO ()
showHandler cf opt = shelly $ print_stdout False $ do
   ls <- runReader getAllAlias cf
   let maxNameLength = maximum $ map T.length ls
   tzs <- genTZs cf opt
   showTZs (isRightAlign opt) maxNameLength tzs

--
genTZs :: ClientConfig -> ShowOpt -> Sh [TZ]
genTZs cf opt = do
   ls <- runReader getAllAlias cf
   xs <- sequence $ map ((flip runReader) cf . getKeys) ls
   return $ Maybe.catMaybes
          $ map (uncurry genTZ)
          $ zip ls $ map (map T.words . T.lines) xs



--
showTZs :: Bool -> Int -> [TZ] -> Sh ()
showTZs b m (x : xs) = do
   echo (showTZ b m x)
   showTZs b m xs
showTZs _ _ [] = return ()
--
showTZ :: Bool -> Int -> TZ -> T.Text
showTZ b m (TZ n a k) = T.concat [(spacing b m n)," < ",a," | ",k," >"]
--
spacing :: Bool -> Int -> T.Text -> T.Text
spacing b m name  =
   let nm = T.length name
       foo = if b then flip else id
   in (foo T.append) name $ T.pack (take (m - nm) $ repeat ' ')
--
