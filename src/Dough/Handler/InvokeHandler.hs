{-# LANGUAGE OverloadedStrings #-}
--
module Dough.Handler.InvokeHandler where
--
import Shelly
import System.Exit
import Debug.Trace
--
import Data.Maybe
import Data.Either
import Control.Monad
import Control.Monad.Reader
--
import Tezos.Data
import Tezos.Client
--
import Dough.Args.OptArg
import Dough.Utils.TzNameData
import Dough.Handler.InquiryHandler
import Dough.Handler.ShowHandler
--
import qualified Data.Text as T
import Text.Read (readMaybe)
--
--
-- tn-register
-- "#key" | "#signature" | "#bytes"
-- "#key" | "#bytes"
-- tn-renew
-- "#key" | "#signature" | "#bytes"
-- tn-free
-- "#key" | "#signature" | "#bytes"
-- tn-updateRoot
-- "#address"
-- tn-updateAdmin
-- "#key" | "#bytes"
--

invokeHandler :: ClientConfig -> InvokeOpt -> IO ()
invokeHandler cf opt = do
   putStrLn "[\ESC[32m\STXRUN\ESC[0m] dough invoke"
   (dispatch cf opt) =<< loadInfo (infoPath opt)

   -- shelly $ print_stdout False $ do
   --    raw <- (flip runReader) cf $ do
   --       nodeInfo <- specNode
   --       -- storageInfo <- getStorage
   --       runTzClient $ nodeInfo <>
   --       -- storageInfo <> [T.pack $ invText opt]
   --    let ptclName = protocol cf
   --    let wss = storagePreprocess raw
   --    -- inspect wss
   --    if ((wss !! 0) !! 0) == ("\""<>ptclName<>"\"")
   --       then do
   --          let tnEntity = unsafeToTzNameData wss
   --          return tnEntity
   --       else do
   --          error ("cannot convert this storage cannot be converted as "++(T.unpack ptclName))
   --
--
dispatch :: ClientConfig -> InvokeOpt -> InvokeArgs -> IO ()
dispatch cf opt (InvokeArgs ivkTrnas (TNS_Register newOwnerName)) = do
   --
   let contractName = T.pack $ contName opt
   let tf = T.pack $ show $ ivkTrnas
   let bc = T.pack $ show $ burnCap cf
   -- get storage
   let iopt = InquiryOpt (configPath_invoke opt) (T.unpack contractName)
   storage <- (either id (error tempInfoErr)) <$> getTzNameStorage cf iopt
   -- get tz list
   let sopt = ShowOpt (configPath_invoke opt) False
   tzs <- shelly $ print_stdout False $ genTZs cf sopt
   --
   let masterName = who cf
   let masterKey = maybe (error "Die: MasterKey") id $ lookupKey tzs masterName
   let masterQuestion = maybe (error "Die: MasterQuestion") id $ lookupQuestion storage masterKey
   newQ <- getNowText cf masterName
   answer <- (T.init . T.drop 11) <$> tzSign cf masterQuestion masterName
   --
   let nownerKey = maybe (error "Die: NewOwnerKey") id $ lookupKey tzs newOwnerName
   nownerQuestion <- getNowText cf newOwnerName
   --
   let info = T.concat ["(Left (Pair (Pair \""
                       , masterKey
                       , "\" (Pair \""
                       , answer
                       , "\" "
                       , newQ
                       , ")) (Pair \""
                       , nownerKey
                       , "\" "
                       , nownerQuestion
                       , ")))" ]
   res <- tzTransfer cf tf masterName contractName info bc
   putStrLn "[\ESC[32m\STXDone\ESC[0m\STX]"
--
dispatch cf opt (InvokeArgs ivkTrnas TNS_Renew) = do
   --
   let contractName = T.pack $ contName opt
   let tf = T.pack $ show $ ivkTrnas
   let bc = T.pack $ show $ burnCap cf
   -- get storage
   let iopt = InquiryOpt (configPath_invoke opt) (T.unpack contractName)
   storage <- (either id (error tempInfoErr)) <$> getTzNameStorage cf iopt
   -- get tz list
   let sopt = ShowOpt (configPath_invoke opt) False
   tzs <- shelly $ print_stdout False $ genTZs cf sopt
   --
   let masterName = who cf
   let masterKey = maybe (error "Die: MasterKey") id $ lookupKey tzs masterName
   let masterQuestion = maybe (error "Die: MasterQuestion") id $ lookupQuestion storage masterKey
   --
   newQ <- getNowText cf masterName
   answer <- (T.init . T.drop 11) <$> tzSign cf masterQuestion masterName
   -- >>> need to match ivkArgs <<<
   let info = T.concat
                  [ "(Right (Left (Pair \""
                  , masterKey
                  , "\" (Pair \""
                  , answer
                  , "\" "
                  , newQ
                  , " ))))"
                  ]
   res <- tzTransfer cf tf masterName contractName info bc
   putStrLn "[\ESC[32m\STXDone\ESC[0m\STX]"
--
dispatch cf opt (InvokeArgs ivkTrnas TNS_Free) = do
   --
   let contractName = T.pack $ contName opt
   let tf = T.pack $ show $ ivkTrnas
   let bc = T.pack $ show $ burnCap cf
   -- get storage
   let iopt = InquiryOpt (configPath_invoke opt) (T.unpack contractName)
   storage <- (either id (error tempInfoErr)) <$> getTzNameStorage cf iopt
   -- get tz list
   let sopt = ShowOpt (configPath_invoke opt) False
   tzs <- shelly $ print_stdout False $ genTZs cf sopt
   --
   let masterName = who cf
   let masterKey = maybe (error "Die: MasterKey") id $ lookupKey tzs masterName
   let masterQuestion = maybe (error "Die: MasterQuestion") id $ lookupQuestion storage masterKey
   --
   newQ <- getNowText cf masterName
   answer <- tzSign cf masterQuestion masterName
   let info = T.concat
                  [ "(Right (Right (Left (Pair \""
                  , masterKey
                  , "\" (Pair \""
                  , T.init $ T.drop 11 answer
                  , "\" "
                  , newQ
                  , " )))))"
                  ]
   res <- tzTransfer cf tf masterName contractName info bc
   putStrLn "[\ESC[32m\STXDone\ESC[0m\STX]"
--
dispatch cf opt (InvokeArgs ivkTrnas (TNS_UpdateRoot newRootAddr)) = do
   case attachType newRootAddr of
      (MiType_Address, nrv) -> do
         let contractName = T.pack $ contName opt
         let masterName = who cf
         let tf = T.pack $ show $ ivkTrnas
         let bc = T.pack $ show $ burnCap cf
         let info = T.concat [ "(Right (Right (Right (Left"
                             , (michelsonFormatWithType nrv)
                             , " ))))" ]
         res <- tzTransfer cf tf masterName contractName info bc
         putStrLn "[\ESC[32m\STXDone\ESC[0m\STX]"
      (_, nrv) -> do
         error $ "[\ESC[31m\STXERROR\ESC[0m\STX]\n    "
                  ++ (T.unpack nrv) ++ " is an invalid tz-address"
--
dispatch cf opt (InvokeArgs ivkTrnas (TNS_updateAdmin newAdminName)) = do
   let sopt = ShowOpt (configPath_invoke opt) False
   tzs <- shelly $ print_stdout False $ genTZs cf sopt
   let masterName = newAdminName
   let masterKey = maybe (error "Die: MasterKey") id $ lookupKey tzs masterName
   newQ <- getNowText cf masterName
   let contractName = T.pack $ contName opt
   let tf = T.pack $ show $ ivkTrnas
   let bc = T.pack $ show $ burnCap cf
   let info = T.concat [ "(Right (Right (Right (Right (Pair \""
                       , masterKey
                       , "\" "
                       , newQ
                       , " )))))" ]
   res <- tzTransfer cf tf (who cf) contractName info bc
   putStrLn "[\ESC[32m\STXDone\ESC[0m\STX]"

--
tempInfoErr = "[\ESC[31m\STXERROR\ESC[0m\STX]\n" ++
   "    Currently \ESC[4m\STXdough inkove\ESC[0m\STX supports TzName contract only"
--
loadInfo :: String -> IO InvokeArgs
loadInfo path = do
   content <- readFile path
   let raw' = (readMaybe content) :: Maybe InvokeArgs
   case raw' of
      Nothing -> die $
         "[\ESC[1;37;41m\STXERROR\ESC[0m\STX] Cannot be parsed into InvokeArgs"
      (Just raw) -> return raw

--
data InkEntry
   = TNS_Register    { newOwner :: T.Text}
   | TNS_Renew
   | TNS_Free
   | TNS_UpdateRoot  { newRoot :: T.Text }
   | TNS_updateAdmin { newAdmin :: T.Text }
   deriving (Show, Read)
--
data InvokeArgs = InvokeArgs
   { ivkTransfer :: Int
   -- , ivkCaller   :: T.Text
   , ivkInfo     :: InkEntry
   } deriving Read
--
