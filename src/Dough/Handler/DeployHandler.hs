{-# LANGUAGE OverloadedStrings #-}
--
module Dough.Handler.DeployHandler where
--
import Shelly
--
import Control.Monad
import Control.Monad.Reader
--
import Dough.Args.OptArg
--
import Dough.Utils.TzNameData
--
import Tezos.Data
import Tezos.Client
--
import qualified Data.Text as T
--
deployHandler :: ClientConfig -> DeployOpt -> IO ()
deployHandler cf opt = do
   raw <- loadData (dataFile opt) -- TzNameData
   let tzName = sTzName raw
   expr <- tzNameData2Expr cf raw
   let ctx = michelsonDataRender expr
   let masterName = who cf
   -- check if there is conflit of TzName
   -- >> to be done
   -- originate TzName
   shelly $ print_stdout False $ do
      res <- (flip runReader) cf $ do
         nodeInfo <- specNode
         runTzClient $ nodeInfo
            <> ["originate", "contract", T.pack $ nameContract opt]
            <> ["for", masterName]
            <> ["transferring", "0"]
            <> ["from", masterName]
            <> ["running", T.pack $ tzFile opt]
            <> ["--init", ctx]
            <> ["--burn-cap", "10"]
      let originatedAddr = getAddrFromOrigination res
      echo $ T.concat [ "[\ESC[32m\STXINFO\ESC[0m\STX]\n "
                       , "    TzName contract:\ESC[33m\STX"
                       , (T.pack $ nameContract opt)
                       , "\ESC[0m\STX of \ESC[33m\STX"
                       , (T.pack $ tzFile opt)
                       , "\n    \ESC[0m\STX has been deploied successfully at address: \ESC[4;32m\STX"
                       , originatedAddr
                       , "\ESC[0m\STX"]
      -- updating record on TNRS
      let newRecord = T.concat ["(Right (Pair \"", tzName, "\" \"", originatedAddr, "\"))"]
      res <- shelly $ print_stdout False $ (flip runReader) cf $ do
         nodeInfo <- specNode
         runTzClient $ nodeInfo
            <> ["transfer", "0"]
            <> ["from", masterName, "to", cTNRSAddr cf]
            <> ["--arg", newRecord]
            <> ["--burn-cap", "10"]
      echo $ T.concat [ "[\ESC[32m\STXINFO\ESC[0m\STX]\n "
                       , "    This origination record has been updated onto \ESC[33m\STX"
                       , cTNRSAddr cf, "\ESC[0m\STX"]
