{-# LANGUAGE OverloadedStrings #-}
--
module Dough.Handler.InquiryHandler where
--
import Shelly
--
import System.IO
--
import Control.Monad
import Control.Monad.Reader
--
import Dough.Args.OptArg
import Dough.Utils.TzNameData
import Tezos.Data
import Tezos.Client
--
--
import qualified Data.Text as T
--
inquiryHandler :: ClientConfig -> InquiryOpt -> IO ()
inquiryHandler cf opt = do
   putStrLn "[\ESC[32m\STXRUN\ESC[0m] dough inquiry"
   lrData <- getTzNameStorage cf opt
   putStrLn "[\ESC[32m\STXINFO\ESC[0m\STX]"
   case lrData of
      (Left  tnData)  -> do
         putStrLn "[\ESC[37m\STXTzName info:\ESC[0m\STX]"
         putStrLn $ show tnData
      (Right tnrData) -> do
         putStrLn "[\ESC[37m\STXTNRS info:\ESC[0m\STX]"
         putStrLn $ show tnrData
--
getTzNameStorage :: ClientConfig -> InquiryOpt -> IO (Either TzNameData TNRecordData)
getTzNameStorage cf opt = do
   shelly $ print_stdout False $ do
      raw <- (flip runReader) cf $ do
         nodeInfo <- specNode
         storageInfo <- getStorage
         runTzClient $ nodeInfo <> storageInfo <> [T.pack $ inqText opt]
      let wss = storagePreprocess raw
      let ptclTNSName = ptclTNS cf
      let ptclTNRSName = ptclTNRS cf
      let headerInfo = (wss !! 0) !! 0
      if headerInfo == ("\"" <> ptclTNSName <> "\"")
         then return $ Left $ unsafeToTzNameData wss
         else if headerInfo == ("\"" <> ptclTNRSName <> "\"")
            then return $ Right $ unsafeToTNRecordData wss
            else error $
               "cannot convert this storage cannot be converted as "
               <> (T.unpack ptclTNSName)
               <> " or "
               <> (T.unpack ptclTNRSName)
