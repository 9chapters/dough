{-# LANGUAGE OverloadedStrings #-}
--
module Main where
--
import Dough
import Tezos.Client
--
import Shelly
import Options.Applicative (execParser)
import System.IO
import System.Directory (getHomeDirectory)
--
import qualified Data.Text as T
import Text.Read (readMaybe)
--
main :: IO ()
main = do
   putStrLn name
   putStrLn welmsg
   x <- execParser opts
   -- homePath <- getHomeDirectory
   let homePath = ""
   let cfPath = if ((getCFPath x) == "")
                  then (homePath ++ "./dough.config")
                  else (getCFPath x)
   configContent <- readFile cfPath
   let cf' = (readMaybe configContent) :: Maybe ClientConfig
   case (cf', x) of
      (Just cf, ShowCmd    cmd) -> showHandler    cf cmd
      (Just cf, InquiryCmd cmd) -> inquiryHandler cf cmd
      (Just cf, DataCmd    cmd) -> dataHandler    cf cmd
      (Just cf, InvokeCmd  cmd) -> invokeHandler  cf cmd
      (Just cf, DeployCmd  cmd) -> deployHandler  cf cmd
      (Nothing, _) -> putStrLn $
         "\ESC[1;37;41m\STX" ++ "[Cannot detect or parse config file: "++
            (show cfPath)++"]" ++ "\ESC[0m\STX"
   -- putStrLn "[\ESC[32m\STXTerminating \ESC[4;33m\STXDough\ESC[32m\STX ..\ESC[0m\STX]"
--

welmsg = "  "
   <> "\ESC[0;44m\STX"
   <> " The "
   <> "\ESC[1;32;44m\STX"
   <> "teznames"
   <> "\ESC[0;44m\STX"
   <> " specific version "
   <> "\ESC[0m\STX"

name = "\ESC[1m\STX"
   <> "       _                   _   " <> "\n"
   <> "    __| | ___  _   _  __ _| |__" <> "\n"
   <> "   / _` |/ _ \\| | | |/ _` | '_ \\" <> "\n"
   <> "  | (_| | (_) | |_| | (_| | | | |" <> "\n"
   <> "   \\__,_|\\___/ \\__,_|\\__, |_| |_|" <> "\n"
   <> "                     |___/"
   <> "\ESC[0m\STX"
